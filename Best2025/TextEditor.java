import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DefaultCaret;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalTime;
import java.awt.geom.Rectangle2D;


public class TextEditor extends JFrame implements ActionListener {
    private JTextPane textPane;
    private JTextArea lineNumbers;
    private JLabel statusBar;
    private File currentFile;
    private static List<TextEditor> openEditors = new ArrayList<>();
    private static int offset = 30;
    private Timer autosaveTimer;
    private boolean darkMode = false;

    public TextEditor() {
        setTitle("Untitled - Notepad");
        setSize(800, 600);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        textPane = new JTextPane();
        textPane.setFont(new Font("Consolas", Font.PLAIN, 14));

        lineNumbers = new JTextArea("1\n");
        lineNumbers.setEditable(false);
        lineNumbers.setBackground(Color.LIGHT_GRAY);
        lineNumbers.setFont(new Font("Consolas", Font.PLAIN, 14));

        JScrollPane scrollPane = new JScrollPane(textPane);
        scrollPane.setRowHeaderView(lineNumbers);

        add(scrollPane, BorderLayout.CENTER);

        textPane.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                SwingUtilities.invokeLater(() -> updateLineNumbers());
            }
        });

        textPane.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateLineNumbers();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateLineNumbers();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateLineNumbers();
            }
        });

        updateLineNumbers();

        statusBar = new JLabel(" Line: 1 | Column: 1 ");
        statusBar.setBorder(BorderFactory.createEtchedBorder());
        add(statusBar, BorderLayout.SOUTH);
        textPane.addCaretListener(e -> updateStatusBar());

        autosaveTimer = new Timer(5000, e -> autosave());
        autosaveTimer.start();

        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        createMenuItem(fileMenu, "New", KeyEvent.VK_N);
        createMenuItem(fileMenu, "Open", KeyEvent.VK_O);
        createMenuItem(fileMenu, "Save", KeyEvent.VK_S);
        createMenuItem(fileMenu, "Save As...", 0);
        createMenuItem(fileMenu, "Close", KeyEvent.VK_W);
        fileMenu.addSeparator();
        createMenuItem(fileMenu, "Quit", KeyEvent.VK_Q);
        menuBar.add(fileMenu);

        JMenu editMenu = new JMenu("Edit");
        createMenuItem(editMenu, "Find/Replace", KeyEvent.VK_F);
        createMenuItem(editMenu, "Word Count", KeyEvent.VK_C); 
        JMenuItem darkModeItem = createMenuItem(editMenu, "Toggle Dark Mode", KeyEvent.VK_D);
        darkModeItem.addActionListener(e -> toggleDarkMode());
        menuBar.add(editMenu);

        JMenu helpMenu = new JMenu("Help");
        createMenuItem(helpMenu, "About", 0);
        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                openEditors.remove(TextEditor.this);
                if (openEditors.isEmpty()) {
                    System.exit(0);
                }
                if (autosaveTimer!= null) {
                    autosaveTimer.stop();
                }
            }
        });

        openEditors.add(this);
        
                // --- Add this code to calculate and set window location ---
        int editorIndex = openEditors.size();
        int xPos = offset * editorIndex;
        int yPos = offset * editorIndex;

        // Get screen dimensions to avoid going off-screen (optional but good practice)
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int maxWidth = screenSize.width - getWidth();
        int maxHeight = screenSize.height - getHeight();

        // Make sure the window stays within screen bounds
        xPos = Math.min(xPos, maxWidth);
        yPos = Math.min(yPos, maxHeight);
        xPos = Math.max(xPos, 0); // Ensure position is not negative
        yPos = Math.max(yPos, 0);

        setLocation(xPos, yPos);
        // --- End of location setting code ---

        setVisible(true); // Make the window visible AFTER setting location
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();

        switch (command) {
            case "New":
                new TextEditor().setVisible(true);  
                break;
            case "Open":
                openFile();
                break;
            case "Save":
                saveFile();
                break;
            case "Save As...":
                saveFileAs();
                break;
            case "Close":
                closeWindow();
                break;
            case "Quit":
                quitApplication();
                break;
            case "About":
                showAboutDialog();
                break;
            case "Find/Replace":
                showFindAndReplaceDialog();
                break;
            case "Word Count":
                showWordCount();
                break;
            default:
                break;
        }
    }

private void openFile() {
    if (confirmSave()) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Text Files (*.txt)", "txt"));

        int option = fileChooser.showOpenDialog(this);
        if (option == JFileChooser.APPROVE_OPTION) {
            currentFile = fileChooser.getSelectedFile();
            try (BufferedReader reader = new BufferedReader(new FileReader(currentFile))) {
                textPane.read(reader, null);
                setTitle(currentFile.getName() + " - Notepad");
                SwingUtilities.invokeLater(() -> updateLineNumbersAsync());
            } catch (IOException ex) {
                showError("Error opening file.");
            }
        }
    }
}

private void saveFile() {
    if (currentFile == null) {
        saveFileAs();
    } else {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(currentFile))) {
            textPane.write(writer);
            setTitle(currentFile.getName() + " - Notepad");
        } catch (IOException ex) {
            showError("Error saving file.");
        }
    }
}


private void saveFileAs() {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFileFilter(new FileNameExtensionFilter("Text Files (*.txt)", "txt"));

    // Get the original filename without the ".txt" extension if it exists
    String baseName = currentFile != null ? currentFile.getName().replace(".txt", "") : "Untitled";

    // Get selected text (if any)
    String selectedText = textPane.getSelectedText();

    // Generate a timestamp for the current date and time
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
    String timestamp = dateFormat.format(new Date());

    // Create a default filename with the original name, timestamp, and selected text (if any)
    String defaultFilename = baseName + "_" + timestamp;

    if (selectedText != null && !selectedText.trim().isEmpty()) {
        // Truncate selected text to the first 20 characters and remove non-alphanumeric characters
        String snippet = selectedText.trim().replaceAll("[^a-zA-Z0-9]", "");
        snippet = snippet.length() > 20 ? snippet.substring(0, 20) : snippet;
        defaultFilename += "_" + snippet;
    }

    defaultFilename += ".txt"; // Add the .txt extension

    // Set the default filename in the file chooser
    fileChooser.setSelectedFile(new File(defaultFilename));

    int option = fileChooser.showSaveDialog(this);
    if (option == JFileChooser.APPROVE_OPTION) {
        currentFile = fileChooser.getSelectedFile();
        // Ensure the file name ends with .txt
        if (!currentFile.getName().endsWith(".txt")) {
            currentFile = new File(currentFile.getAbsolutePath() + ".txt");
        }
        saveFile();
    }
}


// Helper method to get current date and time in a formatted way
private String getCurrentDateTime() {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
    return dateFormat.format(new Date());
}

private void closeWindow() {
    if (confirmSave()) {
        autosaveTimer.stop();
        openEditors.remove(this);
        dispose();
    }
}

private void quitApplication() {
    for (TextEditor editor : openEditors) {
        editor.dispose();
    }
}

private void showFindAndReplaceDialog() {
    JDialog dialog = new JDialog(this, "Find and Replace", true);
    dialog.setSize(400, 200);
    dialog.setLayout(new GridLayout(5, 2, 10, 10)); 
    dialog.setLocationRelativeTo(this);

    JLabel findLabel = new JLabel("Find:");
    JTextField findField = new JTextField();
    JLabel replaceLabel = new JLabel("Replace with:");
    JTextField replaceField = new JTextField();
    JButton findButton = new JButton("Find");
    JButton findNextButton = new JButton("Find Next"); 
    JButton replaceButton = new JButton("Replace");
    JButton replaceAllButton = new JButton("Replace All");
    JButton cancelButton = new JButton("Cancel");

    dialog.add(findLabel);
    dialog.add(findField);
    dialog.add(replaceLabel);
    dialog.add(replaceField);
    dialog.add(findButton);
    dialog.add(findNextButton); 
    dialog.add(replaceButton);
    dialog.add(replaceAllButton);
    dialog.add(cancelButton);

    String selectedText = textPane.getSelectedText();
    if (selectedText!= null &&!selectedText.isEmpty()) {
        findField.setText(selectedText);
    }

    findButton.addActionListener(e -> findText(findField.getText(), true));

    findNextButton.addActionListener(e -> findText(findField.getText(), false));

    replaceButton.addActionListener(e -> {
        String findText = findField.getText();
        String replaceText = replaceField.getText();
        String content = textPane.getText();

        int start = textPane.getSelectionStart();
        int end = textPane.getSelectionEnd();
        String selected = textPane.getSelectedText();

        if (selected!= null && selected.equals(findText)) {
            textPane.replaceSelection(replaceText);
            findText(findText, false); 
        } else {
            JOptionPane.showMessageDialog(dialog, "Text not found or not selected.", "Replace", JOptionPane.INFORMATION_MESSAGE);
        }
    });

    replaceAllButton.addActionListener(e -> {
        String findText = findField.getText();
        String replaceText = replaceField.getText();
        String content = textPane.getText();

        if (content.contains(findText)) {
            content = content.replace(findText, replaceText);
            textPane.setText(content);
            JOptionPane.showMessageDialog(dialog, "All occurrences replaced.", "Replace All", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(dialog, "Text not found.", "Replace All", JOptionPane.INFORMATION_MESSAGE);
        }
    });

    cancelButton.addActionListener(e -> dialog.dispose());

    dialog.setVisible(true);
}

private void findText(String findText, boolean resetSearch) {
    if (findText.isEmpty()) {
        JOptionPane.showMessageDialog(this, "Please enter text to find.", "Find", JOptionPane.WARNING_MESSAGE);
        return;
    }

    String content = textPane.getText();
    int start = resetSearch? 0 : textPane.getSelectionEnd(); 

    int foundIndex = content.indexOf(findText, start);

    if (foundIndex!= -1) {
        textPane.setSelectionStart(foundIndex);
        textPane.setSelectionEnd(foundIndex + findText.length());
        textPane.grabFocus(); 
    } else {
        if (resetSearch) {
            JOptionPane.showMessageDialog(this, "Text not found.", "Find", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "No more occurrences found.", "Find", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}

private JMenuItem createMenuItem(JMenu menu, String title, int keyEvent) {
    JMenuItem menuItem = new JMenuItem(title);
    if (keyEvent!= 0) {
        menuItem.setAccelerator(KeyStroke.getKeyStroke(keyEvent, KeyEvent.CTRL_DOWN_MASK));
    }
    menuItem.addActionListener(this);
    menu.add(menuItem);
    return menuItem;
}

private void updateLineNumbers() {
    SwingUtilities.invokeLater(() -> {
        try {
            javax.swing.text.Element root = textPane.getDocument().getDefaultRootElement();
            int totalLines = root.getElementCount();

            StringBuilder lineNumbersText = new StringBuilder();

            for (int i = 0; i < totalLines; i++) {
                int startOffset = root.getElement(i).getStartOffset();
                int endOffset = root.getElement(i).getEndOffset();

                try {
                    Rectangle2D startRect2D = textPane.modelToView2D(startOffset); // Use modelToView2D
                    Rectangle2D endRect2D = textPane.modelToView2D(endOffset - 1); // Use modelToView2D

                    // Get Y coordinates from Rectangle2D, casting to int if needed
                    int startY = (int) startRect2D.getY();
                    int endY = (int) endRect2D.getY();

                    int lineHeight = textPane.getFontMetrics(textPane.getFont()).getHeight();
                    int visualLines = (endY - startY) / lineHeight + 1;

                    lineNumbersText.append(i + 1).append("\n");

                    for (int j = 1; j < visualLines; j++) {
                        lineNumbersText.append("\n");
                    }
                } catch (BadLocationException ex) {
                    System.err.println("Error calculating line numbers: " + ex.getMessage());
                }
            }

            lineNumbers.setText(lineNumbersText.toString());

            lineNumbers.setPreferredSize(new Dimension(50, textPane.getPreferredSize().height));
            lineNumbers.revalidate();
            lineNumbers.repaint();

        } catch (Exception ex) {
            System.err.println("Error calculating line numbers: " + ex.getMessage());
        }
    });
}

private void updateLineNumbersAsync() {
    SwingWorker<Void, Void> worker = new SwingWorker<>() {
        @Override
        protected Void doInBackground() {
            updateLineNumbers();
            return null;
        }
    };
    worker.execute();
}

private int getWrappedLineCount() throws BadLocationException {
    int lineCount = 0;
    int offset = 0;

    javax.swing.text.Element root = textPane.getDocument().getDefaultRootElement();
    javax.swing.text.View rootView = textPane.getUI().getRootView(textPane).getView(0);

    while (offset < textPane.getDocument().getLength()) {
        int endOfLine = javax.swing.text.Utilities.getRowEnd(textPane, offset);
        lineCount++;

        if (rootView!= null) {
            Rectangle2D startRect2D = textPane.modelToView2D(offset); // Use modelToView2D
            Rectangle2D endRect2D = textPane.modelToView2D(endOfLine); // Use modelToView2D

            // Get Y coordinates from Rectangle2D, casting to int if needed
            int startY = (int) startRect2D.getY();
            int endY = (int) endRect2D.getY();
            lineCount += (endY - startY) / textPane.getFontMetrics(textPane.getFont()).getHeight();
        }

        offset = endOfLine + 1;
    }

    return lineCount;
}

private void updateStatusBar() {
    try {
        int caretPos = textPane.getCaretPosition();
        int line = textPane.getDocument().getDefaultRootElement().getElementIndex(caretPos) + 1;
        int column = caretPos - textPane.getDocument().getDefaultRootElement().getElement(line - 1).getStartOffset() + 1;
        statusBar.setText(" Line: " + line + " | Column: " + column + " | Words: " + countWords());
    } catch (Exception ex) {
        statusBar.setText(" Line: 1 | Column: 1 ");
    }
}

private int countWords() {
    String text = textPane.getText().trim();
    return text.isEmpty()? 0 : text.split("\\s+").length;
}


private void autosave() {
    try {
        File autosaveFile = new File("~notepad.autosave.txt");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(autosaveFile))) {
            writer.write(textPane.getText());
        }
        statusBar.setText("Autosaved at: " + LocalTime.now());
    } catch (IOException ex) {
        JOptionPane.showMessageDialog(this, "Autosave failed: " + ex.getMessage(), "Autosave Error", JOptionPane.ERROR_MESSAGE);
    }
}

private void toggleDarkMode() {
    darkMode =!darkMode;
    textPane.setBackground(darkMode? Color.DARK_GRAY : Color.WHITE);
    textPane.setForeground(darkMode? Color.WHITE : Color.BLACK);
    lineNumbers.setBackground(darkMode? Color.DARK_GRAY : Color.LIGHT_GRAY);
    lineNumbers.setForeground(darkMode? Color.WHITE : Color.BLACK);
    statusBar.setBackground(darkMode? Color.DARK_GRAY : Color.LIGHT_GRAY);
    statusBar.setForeground(darkMode? Color.WHITE : Color.BLACK);
    statusBar.setOpaque(true); 

    JMenuBar menuBar = getJMenuBar();
    if (menuBar!= null) {
        menuBar.setBackground(darkMode? Color.DARK_GRAY : Color.LIGHT_GRAY);
        menuBar.setForeground(darkMode? Color.WHITE : Color.BLACK);

        for (int i = 0; i < menuBar.getMenuCount(); i++) {
            JMenu menu = menuBar.getMenu(i);
            if (menu!= null) {
                menu.setBackground(darkMode? Color.DARK_GRAY : Color.LIGHT_GRAY);
                menu.setForeground(darkMode? Color.WHITE : Color.BLACK);

                for (Component component : menu.getMenuComponents()) {
                    if (component instanceof JMenuItem) {
                        component.setBackground(darkMode? Color.DARK_GRAY : Color.LIGHT_GRAY);
                        component.setForeground(darkMode? Color.WHITE : Color.BLACK);
                    }
                }
            }
        }
    }
}

private boolean confirmSave() {
    if (textPane.getText()!= null &&!textPane.getText().isEmpty()) {
        int option = JOptionPane.showConfirmDialog(this, 
            "Do you want to save changes?", "Save", JOptionPane.YES_NO_CANCEL_OPTION);
        if (option == JOptionPane.YES_OPTION) {
            saveFile();
        }
        return option!= JOptionPane.CANCEL_OPTION;
    }
    return true;
}

private void showError(String message) {
    JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
}

private void showAboutDialog() {
    JOptionPane.showMessageDialog(this, "Notepad v0.18\nCreated by Notknown Name", "About", JOptionPane.INFORMATION_MESSAGE);
}

private void showWordCount() {
    int wordCount = countWords();
    JOptionPane.showMessageDialog(this, "Word Count: " + wordCount);
}

public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> new TextEditor().setVisible(true));
}

}
