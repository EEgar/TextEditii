import tkinter as tk
from tkinter import filedialog, messagebox

current_file = None

def new_file():
    global current_file
    text_widget.delete("1.0", tk.END)
    current_file = None

def open_file():
    global current_file
    file_path = filedialog.askopenfilename(filetypes=[("Text files", "*.txt")])
    if file_path:
        with open(file_path, "r") as file:
            text_widget.delete("1.0", tk.END)
            text_widget.insert(tk.END, file.read())
        current_file = file_path

def save_file():
    global current_file
    if current_file:
        with open(current_file, "w") as file:
            file.write(text_widget.get("1.0", tk.END))
    else:
        save_file_as()

def save_file_as():
    global current_file
    file_path = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files", "*.txt")])
    if file_path:
        with open(file_path, "w") as file:
            file.write(text_widget.get("1.0", tk.END))
        current_file = file_path

def exit_editor():
    if messagebox.askokcancel("Exit", "Do you want to exit?"):
        window.destroy()

def cut_text():
    text_widget.event_generate("<<Cut>>")

def copy_text():
    text_widget.event_generate("<<Copy>>")

def paste_text():
    text_widget.event_generate("<<Paste>>")

def show_about():
    messagebox.showinfo("About", "Simple Text Editor\nVersion 1.0\nAuthor: Your Name")

window = tk.Tk()
window.title("Text Editor")

menu_bar = tk.Menu(window)

# File menu
file_menu = tk.Menu(menu_bar, tearoff=0)
file_menu.add_command(label="New", command=new_file)
file_menu.add_command(label="Open", command=open_file)
file_menu.add_command(label="Save", command=save_file)
file_menu.add_command(label="Save As", command=save_file_as)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=exit_editor)
menu_bar.add_cascade(label="File", menu=file_menu)

# Edit menu
edit_menu = tk.Menu(menu_bar, tearoff=0)
edit_menu.add_command(label="Cut", command=cut_text, accelerator="Ctrl+X")
edit_menu.add_command(label="Copy", command=copy_text, accelerator="Ctrl+C")
edit_menu.add_command(label="Paste", command=paste_text, accelerator="Ctrl+V")
menu_bar.add_cascade(label="Edit", menu=edit_menu)

# Help menu
help_menu = tk.Menu(menu_bar, tearoff=0)
help_menu.add_command(label="About", command=show_about)
menu_bar.add_cascade(label="Help", menu=help_menu)

window.config(menu=menu_bar)

text_widget = tk.Text(window)
text_widget.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

# Scrollbar
scrollbar = tk.Scrollbar(window, command=text_widget.yview)
scrollbar.pack(side=tk.RIGHT, fill=tk.Y, before=text_widget)
text_widget.config(yscrollcommand=scrollbar.set)


# Status bar
status_bar = tk.Label(window, text="Ln 1, Col 1", bd=1, relief=tk.SUNKEN, anchor=tk.W)
status_bar.pack(side=tk.BOTTOM, fill=tk.X)

# Function to update the status bar with current cursor position
def update_status_bar(event=None):
    cursor_pos = text_widget.index(tk.INSERT)
    line, col = cursor_pos.split(".")
    status_bar.config(text=f"Ln {line}, Col {col}")

text_widget.bind("<KeyRelease>", update_status_bar)
text_widget.bind("<ButtonRelease>", update_status_bar)

window.mainloop()

