import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Element;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public final class TextEditii extends JFrame implements ActionListener {
    private static JTextArea area;
    private static JTextArea lineNumberArea;
    private static JFrame frame;
    private static int returnValue = 0;
    private static JScrollPane scrollPane;
    private static JScrollPane lineNumberScrollPane;
    private static File openedFile = null;

    public TextEditii() {
        run();
    }

    public void run() {
        frame = new JFrame("Text Edit");
        frame.setIconImage(createIcon().getImage());

        // Set the look-and-feel (LNF) of the application
        // Try to default to whatever the host system prefers
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }

        // Set attributes of the app window
        area = new JTextArea();
        
        area.setFont(new Font("Consolas", Font.PLAIN, 20)); // Set font to Consolas, size 14
        //area.setFont(new Font("Arial Unicode MS", Font.PLAIN, 20)); // Use a font that supports Ukrainian Cyrillic
        
        area.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateLineNumbers();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateLineNumbers();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateLineNumbers();
            }
        });

        lineNumberArea = new JTextArea("1");
        lineNumberArea.setBackground(Color.DARK_GRAY);
        lineNumberArea.setForeground(Color.LIGHT_GRAY);
        lineNumberArea.setEditable(false);
        lineNumberArea.setMargin(new Insets(0, 5, 0, 5));

        scrollPane = new JScrollPane(area);
        lineNumberScrollPane = new JScrollPane(lineNumberArea);
        lineNumberScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);

        JPanel contentPane = new JPanel(new BorderLayout());
        contentPane.add(scrollPane, BorderLayout.CENTER);
        contentPane.add(lineNumberScrollPane, BorderLayout.WEST);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(contentPane);
        frame.setJMenuBar(createMenuBar());
        frame.setSize(640, 480);
        frame.setVisible(true);

        // Add a listener to synchronize the scrolling of the line number view with the main text area
        scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                lineNumberScrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getValue());
            }
        });
    }

    private JMenuBar createMenuBar() {
        // Build the menu
        JMenuBar menu_main = new JMenuBar();

        JMenu menu_file = new JMenu("File");

        JMenuItem menuitem_new = new JMenuItem("New");
        JMenuItem menuitem_open = new JMenuItem("Open");
        JMenuItem menuitem_save = new JMenuItem("Save...");
        JMenuItem menuitem_quit = new JMenuItem("Quit");
        JMenuItem menuitem_close = new JMenuItem("Close File");

        menuitem_new.addActionListener(this);
        menuitem_open.addActionListener(this);
        menuitem_save.addActionListener(this);
        menuitem_quit.addActionListener(this);
        menuitem_close.addActionListener(this);

        menu_main.add(menu_file);

        menu_file.add(menuitem_new);
        menu_file.add(menuitem_open);
        menu_file.add(menuitem_save);
        menu_file.add(menuitem_close);
        menu_file.add(menuitem_quit);

        JMenu menu_help = new JMenu("Help");
        JMenuItem menuitem_about = new JMenuItem("About");

        menuitem_about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAboutDialog();
            }
        });

        menu_main.add(menu_help);
        menu_help.add(menuitem_about);

        return menu_main;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String ingest = null;
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        jfc.setDialogTitle("Choose destination.");
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        String ae = e.getActionCommand();
        if (ae.equals("Open")) {
            returnValue = jfc.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File f = new File(jfc.getSelectedFile().getAbsolutePath());
                try {
                    FileReader read = new FileReader(f);
                    Scanner scan = new Scanner(read);
                    while (scan.hasNextLine()) {
                        String line = scan.nextLine() + "\n";
                        ingest = ingest + line;
                    }
                    area.setText(ingest);
                    openedFile = f;
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
            // SAVE
        } else if (ae.equals("Save...")) {
            returnValue = jfc.showSaveDialog(null);
            try {
                File f = new File(jfc.getSelectedFile().getAbsolutePath());
                FileWriter out = new FileWriter(f);
                out.write(area.getText());
                out.close();
            } catch (FileNotFoundException ex) {
                Component f = null;
                JOptionPane.showMessageDialog(f, "File not found.");
            } catch (IOException ex) {
                Component f = null;
                JOptionPane.showMessageDialog(f, "Error.");
            }
        } else if (ae.equals("New")) {
            area.setText("");
            openedFile = null;
        } else if (ae.equals("Quit")) {
            System.exit(0);
        } else if (ae.equals("Close File")) {
            area.setText("");
            openedFile = null;
        }
    }

    private void updateLineNumbers() {
        // Get the current number of lines in the document
        int totalLines = area.getLineCount();

        // Create a StringBuilder to hold the line numbers
        StringBuilder lineNumbers = new StringBuilder();

        // Append line numbers from 1 to totalLines to the StringBuilder
        for (int i = 1; i <= totalLines; i++) {
            lineNumbers.append(i).append("\n");
        }

        // Set the updated line numbers text in the lineNumberArea
        lineNumberArea.setText(lineNumbers.toString());

        // Set the preferred size of the lineNumberArea based on the number of lines
        FontMetrics fontMetrics = lineNumberArea.getFontMetrics(lineNumberArea.getFont());
        int lineHeight = fontMetrics.getHeight();
        int preferredWidth = fontMetrics.stringWidth(String.valueOf(totalLines)) + 10;
        int preferredHeight = area.getHeight();
        lineNumberScrollPane.setPreferredSize(new Dimension(preferredWidth, preferredHeight));

        // Repaint the lineNumberScrollPane to reflect the changes
        lineNumberScrollPane.revalidate();
        lineNumberScrollPane.repaint();
    }

    private ImageIcon createIcon() {
        // Icon image encoded as Base64
        String base64IconImage = "iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAYAAACAvzbMAAAA0GVYSWZJSSoACAAAAAoAAAEEAAEAAACQAQAAAQEEAAEAAACQAQAAAgEDAAMAAACGAAAAEgEDAAEAAAABAAAAGgEFAAEAAACMAAAAGwEFAAEAAACUAAAAKAEDAAEAAAACAAAAMQECAA0AAACcAAAAMgECABQAAACqAAAAaYcEAAEAAAC+AAAAAAAAAAgACAAIAEgAAAABAAAASAAAAAEAAABHSU1QIDIuMTAuMzQAADIwMjM6MDc6MDEgMTU6NDQ6NTYAAQABoAMAAQAAAAEAAAAAAAAAbTCL4wAAAYRpQ0NQSUNDIHByb2ZpbGUAAHicfZE9SMNQFIVPU6UiFQc7iDhkqJ3aRUUcaxWKUCHUCq06mLz0D5o0JCkujoJrwcGfxaqDi7OuDq6CIPgD4uripOgiJd6XFFrEeOHxPs675/DefYDQqjHN6ksCmm6b2XRKzBdWxdArQgggjDhiMrOMOUnKwLe+7qmX6i7Bs/z7/qwhtWgxICASJ5lh2sQbxDObtsF5nzjCKrJKfE4cN+mCxI9cVzx+41x2WeCZETOXnSeOEIvlHlZ6mFVMjXiaOKpqOuULeY9VzluctVqDde7JXxgu6ivLXKc1jjQWsQQJIhQ0UEUNNhK066RYyNJ5ysc/5volcinkqoKRYwF1aJBdP/gf/J6tVZqa9JLCKaD/xXE+JoDQLtBuOs73seO0T4DgM3Cld/31FjD7SXqzq0WPgOFt4OK6qyl7wOUOMPpkyKbsSkFaQqkEvJ/RNxWAkVtgcM2bW+ccpw9AjmaVuQEODoFYmbLXfd490Du3f3s68/sBmbhytsInzOIAAA14aVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/Pgo8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA0LjQuMC1FeGl2MiI+CiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiCiAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICB4bWxuczpHSU1QPSJodHRwOi8vd3d3LmdpbXAub3JnL3htcC8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgeG1wTU06RG9jdW1lbnRJRD0iZ2ltcDpkb2NpZDpnaW1wOmMyN2M3YzNmLTY0YzAtNDQxMC05NDNjLTA2YjNhZjRkNmIxOSIKICAgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3OWU1MTk3ZC1hZTBkLTQ1NjEtOTA5My03OGQzMTIwZjA0YWIiCiAgIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDoyYWU2OThmNi1hYWRmLTQ0ODgtYmNmMC0wOTdmY2JjZWJlMjYiCiAgIGRjOkZvcm1hdD0iaW1hZ2UvcG5nIgogICBHSU1QOkFQST0iMi4wIgogICBHSU1QOlBsYXRmb3JtPSJMaW51eCIKICAgR0lNUDpUaW1lU3RhbXA9IjE2ODgyMTU0OTY3MjY4NDciCiAgIEdJTVA6VmVyc2lvbj0iMi4xMC4zNCIKICAgdGlmZjpPcmllbnRhdGlvbj0iMSIKICAgeG1wOkNyZWF0b3JUb29sPSJHSU1QIDIuMTAiCiAgIHhtcDpNZXRhZGF0YURhdGU9IjIwMjM6MDc6MDFUMTU6NDQ6NTYrMDM6MDAiCiAgIHhtcDpNb2RpZnlEYXRlPSIyMDIzOjA3OjAxVDE1OjQ0OjU2KzAzOjAwIj4KICAgPHhtcE1NOkhpc3Rvcnk+CiAgICA8cmRmOlNlcT4KICAgICA8cmRmOmxpCiAgICAgIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiCiAgICAgIHN0RXZ0OmNoYW5nZWQ9Ii8iCiAgICAgIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6ZTBhODQzNjYtNjNiMi00ZTNkLTliZTYtMTJmNTViODVmMDY2IgogICAgICBzdEV2dDpzb2Z0d2FyZUFnZW50PSJHaW1wIDIuMTAgKExpbnV4KSIKICAgICAgc3RFdnQ6d2hlbj0iMjAyMy0wNy0wMVQxNTo0NDo1NiswMzowMCIvPgogICAgPC9yZGY6U2VxPgogICA8L3htcE1NOkhpc3Rvcnk+CiAgPC9yZGY6RGVzY3JpcHRpb24+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgCjw/eHBhY2tldCBlbmQ9InciPz75BUAcAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5wcBDCw41SAQuwAABPtJREFUeNrt3NENgzAMRVFTZfC3OV0BRW3A5JwJLNfoKj89quqsJpIUe3MD9spzfKwAAAEBQEAAEBAABAQABAQAAQFAQAAQEAAEBAAEBAABAeAmRzX6M8VOOv05nVnNCl4gAAgIAAICgIAAgIAAICAACAgAAgKAgACAgAAgIAAICAACAoCAAICAACAgAAgIAAICgIAAgIAAICAACAgAAgKAgACAgAAgIAAICAACAoCAAICAACAgAAgIAAICgIAAgIAAICAACAgAAgIAAgKAgAAgIAAICADvNDoNm8Ssm8+KG7DX58zqBQLAFAEBQEAAEBAABAQAAQEAAQFAQAAQEAAEBAABAQABAUBAABAQAAQEAAEBAAEBQEAAEBAABAQAAQEAAQFAQAAQEAAEBAABAQABAUBAABAQAAQEAAEBAAEBQEAAEBAABAQAAQEAAQFAQAAQEAAEBAAEBAABAWCp0WnYJGaFzfm2vEAAEBAABAQABAQAAQFAQAAQEAAEBAAEBAABAUBAABAQABAQAAQEAAEBQEAAEBAAEBAABAQAAQFAQAAQEAAQEAAEBAABAUBAABAQABAQAAQEAAEBQEAAEBAAEBAABAQAAQFAQAAQEAAQEAAEBAABAUBAABAQALjuqKrTGugiiVndgFm9QAAQEAAEBAAEBAABAUBAABAQAAQEAAQEAAEBQEAAEBAABAQABAQAAQFAQAAQEAAQEAAEBAABAUBAABAQABAQAAQEAAEBQEAAEBAAEBAABAQAAQFAQAAQEAAQEAAEBAABAUBAABAQABAQAAQEAAEBQEAAEBAAuGgkaTNsp1k7sVfcgL3OzOoFAsAUAQFAQAAQEAAEBAABAQABAUBAABAQAAQEAAEBAAEBQEAAEBAABAQAAQEAAQFAQAAQEAAEBAABAQABAUBAABAQAAQEAAEBAAEBQEAAEBAABAQAAQEAAQFAQAAQEAAEBAABAQABAUBAABAQAAQEAAQEAAEBYKlhBfAfSSzBDbx6Vi8QAKYICAACAoCAACAgAAgIAAgIAAICgIAAICAACAgACAgAAgKAgAAgIAAICAAICAACAoCAACAgAAgIAAgIAAICgIAAICAACAgACAgAAgKAgAAgIAAICAAICAACAoCAACAgAAgIAAgIAAICgIAAICAAICAACAgASx1VdVrD7yUx6+az4ga8QABAQAAQEAAEBAABAUBAAEBAABAQAAQEAAEBQEAAQEAAEBAABAQAAQFAQABAQAAQEAAEBAABAUBAAEBAABAQAAQEAAEBAAEBQEAAEBAABAQAAQEAAQFAQAAQEAAEBAABAQABAUBAABAQAAQEAAEBAAEBQEAAuNvoNGwSvxju1bfFQ27ACwSAKQICgIAAICAACAgAAgIAAgKAgAAgIAAICAACAgACAoCAACAgAAgIAAICAAICgIAAICAACAgAAgIAAgKAgAAgIAAICAACAgACAoCAACAgAAgIAAICAAICgIAAICAACAgAAgIAAgKAgAAgIAAICAAICAACAsBSI4ktbK7TDbhXeM635QUCwBQBAUBAABAQAAQEAAEBAAEBQEAAEBAABAQAAQEAAQFAQAAQEAAEBAABAQABAUBAABAQAAQEAAEBAAEBQEAAEBAABAQAAQEAAQFAQAAQEAAEBAABAQABAUBAABAQAAQEAAEBAAEBQEAAEBAABAQABAQAAQFgqS9Q/kSlN5eyygAAAABJRU5ErkJggg==";

        // Decode the Base64 image data
        byte[] decodedIconImage = Base64.getDecoder().decode(base64IconImage);

        // Create and return the ImageIcon
        return new ImageIcon(decodedIconImage);
    }

    private void showAboutDialog() {
        String appName = "TextEditii";
        String version = "0.0.4";
        String website = "codeberg.org/EEgar";
        String os = System.getProperty("os.name");
        String memoryUsage = String.format("%.2f MB", (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024.0 * 1024.0));

        StringBuilder message = new StringBuilder();
        message.append("Application Name: ").append(appName).append("\n");
        message.append("Version: ").append(version).append("\n");
        message.append("Website: ").append(website).append("\n");
        message.append("Operating System: ").append(os).append("\n");
        message.append("Memory Usage: ").append(memoryUsage);

        JOptionPane.showMessageDialog(frame, message.toString(), "About", JOptionPane.INFORMATION_MESSAGE);
    }



    public static void main(String[] args) {
        TextEditii runner = new TextEditii();
    }
}
